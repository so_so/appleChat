package com.chat;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.ProgressMonitor;
import javax.swing.ProgressMonitorInputStream;


public class SendFileThread extends Thread {
	
	private ServerSocket serverSocket;
    private File file;
    JFileChooser sourceFileChooser;
    int status;
    Chat frame;

    public SendFileThread(Chat frame,ServerSocket serverSocket, File file, JFileChooser sourceFileChooser, int status) {
        this.serverSocket=serverSocket;
    	this.file = file;
        this.sourceFileChooser = sourceFileChooser;
        this.status = status;
        this.frame = frame;
    }

    public void run() {

    	Socket fileReceiverSocket = null;
		BufferedReader getFromReceiver = null;
		FileInputStream sendFileStream = null;
		ProgressMonitorInputStream monitor = null;
		DataOutputStream sendFileToReceiver = null;
		try {
			//serverSocket.setSoTimeout(1000);
			fileReceiverSocket = serverSocket.accept();

			getFromReceiver = new BufferedReader(new InputStreamReader(fileReceiverSocket.getInputStream()));

			String judge = getFromReceiver.readLine();

			if (judge.equals("accepted")) {
				sendFileToReceiver = new DataOutputStream(
						new BufferedOutputStream(fileReceiverSocket.getOutputStream()));

				sendFileStream = new FileInputStream(file);

				monitor = new ProgressMonitorInputStream(frame, "正在发送： " + file.getName(), sendFileStream);
				ProgressMonitor progressMonitor = monitor.getProgressMonitor();

				int read_unit = 500;
				long fileSize = monitor.available();
				int readed = 0;
				byte[] data = new byte[read_unit];

				sendFileToReceiver.writeByte(1);
				sendFileToReceiver.flush();
				while (monitor.available() > 0) {
					int in = monitor.read(data);
					readed += in;
					float process = (float) readed / fileSize * 100;
					progressMonitor.setNote(process + " % 完成");
					if (in > 0) {
						sendFileToReceiver.write(data, 0, in);
					}
				}
				sendFileToReceiver.flush();

			} else if (judge.equals("refused")) {
				JOptionPane.showMessageDialog(frame, "对方拒绝接受文件 " + file.getName(), "错误！",
						JOptionPane.INFORMATION_MESSAGE);
			}

		} catch (IOException e1) {
			System.out.println("错误");
			e1.printStackTrace();
			// JOptionPane.showMessageDialog(frame, "传输中断!", "错误",
			// JOptionPane.ERROR_MESSAGE);
		} finally {
			if (sendFileStream != null)
				try {
					sendFileStream.close();
					monitor.close();
				} catch (IOException e) {
				}
			if (sendFileToReceiver != null) {
				try {
					sendFileToReceiver.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(fileReceiverSocket!=null){ 
				try {
					fileReceiverSocket.close();
					System.out.println("关闭了文件服务");
				 } catch (IOException e) { 
					 e.printStackTrace();
				 }
			}
			 
		}
    }
}
