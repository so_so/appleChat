package com.chat;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.TitledBorder;

public class LoginFrame extends JFrame{
	
	private JPanel logPanle;
	private JLabel label_ip;
	private JLabel label_port;
	private JLabel label_username;

	private JTextField txt_login_name;
	
	private JTextField txt_login_ip;
	private JTextField txt_login_port;
	private JButton btn_submit;
	
	public LoginFrame(){
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Font font = new Font("宋体", 1, 16);

		logPanle = new JPanel();
		this.setLayout(null);
		//logPanle.setLayout(null);
		
		logPanle.setBounds(0, 0, 295, 260);
		logPanle.setBackground(new Color(220,220,220));
		logPanle.setLayout(new GridLayout(4, 2, 20, 20));
		logPanle.setBorder(new TitledBorder("登陆"));
		
		label_ip = new JLabel("服务器IP:");
		label_ip.setFont(font);
		label_ip.setHorizontalAlignment(SwingConstants.CENTER);
		logPanle.add(label_ip);
		
		txt_login_ip = new JTextField("34.1.172.149");
		txt_login_ip.setFont(font);
		logPanle.add(txt_login_ip);
		
		label_port = new JLabel("服务器端口:");
		label_port.setFont(font);
		label_port.setHorizontalAlignment(SwingConstants.CENTER);
		logPanle.add(label_port);
		
		txt_login_port = new JTextField("6666");
		txt_login_port.setFont(font);
		logPanle.add(txt_login_port);
		
		label_username = new JLabel("昵 称:");
		label_username.setFont(font);
		label_username.setHorizontalAlignment(SwingConstants.CENTER);
		logPanle.add(label_username);
		
		txt_login_name = new JTextField("");
		txt_login_name.setFont(font);
		logPanle.add(txt_login_name);

		
		
		btn_submit = new JButton("登陆");
		btn_submit.setFont(font);
		
		btn_submit.setBounds((logPanle.getWidth()-btn_submit.getWidth())/2,
				(logPanle.getHeight()-btn_submit.getHeight())/2, 
				btn_submit.getWidth(),btn_submit.getHeight());
		logPanle.add(btn_submit);
		
		
		this.setTitle("登陆窗口");
		
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(
				this.getClass().getResource("/resource/2.png")));
		
		this.add(logPanle, "Center");
		this.setSize(300, 300);
		this.setResizable(false);
		int screen_width = Toolkit.getDefaultToolkit().getScreenSize().width;
		int screen_height = Toolkit.getDefaultToolkit().getScreenSize().height;
		this.setLocation((screen_width - this.getWidth()) / 2,
				(screen_height - this.getHeight()) / 2);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.setVisible(true);
		txt_login_name.requestFocus();
	}

	public JPanel getLogPanle() {
		return logPanle;
	}

	public void setLogPanle(JPanel logPanle) {
		this.logPanle = logPanle;
	}

	public JLabel getLabel_username() {
		return label_username;
	}

	public void setLabel_username(JLabel label_username) {
		this.label_username = label_username;
	}


	public JTextField getTxt_login_name() {
		return txt_login_name;
	}

	public void setTxt_login_name(JTextField txt_login_name) {
		this.txt_login_name = txt_login_name;
	}

	
	public JTextField getTxt_login_ip() {
		return txt_login_ip;
	}

	public void setTxt_login_ip(JTextField txt_login_ip) {
		this.txt_login_ip = txt_login_ip;
	}

	public JTextField getTxt_login_port() {
		return txt_login_port;
	}

	public void setTxt_login_port(JTextField txt_login_port) {
		this.txt_login_port = txt_login_port;
	}

	public JButton getBtn_submit() {
		return btn_submit;
	}

	public void setBtn_submit(JButton btn_submit) {
		this.btn_submit = btn_submit;
	}
	
	
	
}
