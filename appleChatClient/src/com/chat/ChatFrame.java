package com.chat;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;


public class ChatFrame extends JFrame {


	private JList userList;
	private TextAreaMenu textArea;
	

	private JTextField txt_port;
	private JTextField txt_hostIp;
	private JTextField txt_name;

	private JButton btn_stop;

	private JPanel northPanel;
	
	private JScrollPane rightScroll;
	private JScrollPane leftScroll;
	private JSplitPane centerSplit;

	private DefaultListModel listModel;

	private int sendfor_who = 0;

	public ChatFrame() {
		
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		SelecTry selectIndex = new SelecTry();

		textArea = new TextAreaMenu();
		textArea.setEditable(false);
		textArea.setForeground(Color.blue);

		
		txt_port = new JTextField("6");
		txt_hostIp = new JTextField("127.0.0.1");
		
		txt_name = new JTextField(" ");// 登陆用户名

		;
		btn_stop = new JButton("退出");
	
		listModel = new DefaultListModel();
		userList = new JList(listModel);

		userList.addListSelectionListener(selectIndex);
		northPanel = new JPanel();
		northPanel.setLayout(new GridLayout(1, 7));
		northPanel.add(new JLabel("     端口"));
		northPanel.add(txt_port);
		northPanel.add(new JLabel("    服务器IP"));
		northPanel.add(txt_hostIp);
		northPanel.add(new JLabel("     用户名"));
		northPanel.add(txt_name);

		northPanel.add(btn_stop);
		northPanel.setBorder(new TitledBorder("连接信息"));

		rightScroll = new JScrollPane(textArea);
		rightScroll.setBorder(new TitledBorder("消息通知"));
		leftScroll = new JScrollPane(userList);
		leftScroll.setBorder(new TitledBorder("用户列表"));
		

		centerSplit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, leftScroll, rightScroll);
		centerSplit.setDividerLocation(150);

		
		// 更改JFrame的图标：
		
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(
				this.getClass().getResource("/resource/2.png")));
		this.setResizable(false);
		this.setLayout(new BorderLayout());
		this.add(northPanel, "North");
		this.add(centerSplit, "Center");
		
		this.setSize(600, 400);
		int screen_width = Toolkit.getDefaultToolkit().getScreenSize().width;
		int screen_height = Toolkit.getDefaultToolkit().getScreenSize().height;
		this.setLocation((screen_width - this.getWidth()) / 2, (screen_height - this.getHeight()) / 2);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(false);

	}

	
	

	public JList getUserList() {
		return userList;
	}

	public void setUserList(JList userList) {
		this.userList = userList;
	}

	public TextAreaMenu getTextArea() {
		return textArea;
	}

	public void setTextArea(TextAreaMenu textArea) {
		this.textArea = textArea;
	}

	
	public JTextField getTxt_port() {
		return txt_port;
	}

	public void setTxt_port(JTextField txt_port) {
		this.txt_port = txt_port;
	}

	public JTextField getTxt_hostIp() {
		return txt_hostIp;
	}

	public void setTxt_hostIp(JTextField txt_hostIp) {
		this.txt_hostIp = txt_hostIp;
	}

	public JTextField getTxt_name() {
		return txt_name;
	}

	public void setTxt_name(JTextField txt_name) {
		this.txt_name = txt_name;
	}

	public JButton getBtn_stop() {
		return btn_stop;
	}

	public void setBtn_stop(JButton btn_stop) {
		this.btn_stop = btn_stop;
	}

	

	public JPanel getNorthPanel() {
		return northPanel;
	}

	public void setNorthPanel(JPanel northPanel) {
		this.northPanel = northPanel;
	}

	
	public JScrollPane getRightScroll() {
		return rightScroll;
	}

	public void setRightScroll(JScrollPane rightScroll) {
		this.rightScroll = rightScroll;
	}

	public JScrollPane getLeftScroll() {
		return leftScroll;
	}

	public void setLeftScroll(JScrollPane leftScroll) {
		this.leftScroll = leftScroll;
	}

	public JSplitPane getCenterSplit() {
		return centerSplit;
	}

	public void setCenterSplit(JSplitPane centerSplit) {
		this.centerSplit = centerSplit;
	}

	public DefaultListModel getListModel() {
		return listModel;
	}

	public void setListModel(DefaultListModel listModel) {
		this.listModel = listModel;
	}

	public int getSendfor_who() {
		return sendfor_who;
	}

	public void setSendfor_who(int sendfor_who) {
		this.sendfor_who = sendfor_who;
	}





	/**
	 * 左边用户列表
	 * @author Administrator
	 *
	 */
	class SelecTry implements ListSelectionListener {
		int change = 0, who;

		public void valueChanged(ListSelectionEvent e) {
			// System.out.println("you
			// selected:"+listModel.getElementAt(userList.getSelectedIndex()));
			sendfor_who = userList.getSelectedIndex();
			// isConnected_p2p = false;
		}

	}

	/**
	 * 带有功能菜单的JTextArea
	 * 
	 */
	class TextAreaMenu extends JTextArea implements MouseListener {

		private static final long serialVersionUID = -2308615404205560110L;

		private JPopupMenu pop = null; // 弹出菜单

		private JMenuItem copy = null, paste = null, cut = null; // 三个功能菜单

		public TextAreaMenu() {
			super();
			init();
		}

		private void init() {
			this.addMouseListener(this);
			pop = new JPopupMenu();
			pop.add(copy = new JMenuItem("复制"));
			pop.add(paste = new JMenuItem("粘贴"));
			pop.add(cut = new JMenuItem("剪切"));
			copy.setAccelerator(KeyStroke.getKeyStroke('C', InputEvent.CTRL_MASK));
			paste.setAccelerator(KeyStroke.getKeyStroke('V', InputEvent.CTRL_MASK));
			cut.setAccelerator(KeyStroke.getKeyStroke('X', InputEvent.CTRL_MASK));
			copy.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					action(e);
				}
			});
			paste.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					action(e);
				}
			});
			cut.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					action(e);
				}
			});
			this.add(pop);
		}

		/**
		 * 菜单动作
		 * 
		 * @param e
		 */
		public void action(ActionEvent e) {
			String str = e.getActionCommand();
			if (str.equals(copy.getText())) { // 复制
				this.copy();
			} else if (str.equals(paste.getText())) { // 粘贴
				this.paste();
			} else if (str.equals(cut.getText())) { // 剪切
				this.cut();
			}
		}

		public JPopupMenu getPop() {
			return pop;
		}

		public void setPop(JPopupMenu pop) {
			this.pop = pop;
		}

		/**
		 * 剪切板中是否有文本数据可供粘贴
		 * 
		 * @return true为有文本数据
		 */
		public boolean isClipboardString() {
			boolean b = false;
			Clipboard clipboard = this.getToolkit().getSystemClipboard();
			Transferable content = clipboard.getContents(this);
			try {
				if (content.getTransferData(DataFlavor.stringFlavor) instanceof String) {
					b = true;
				}
			} catch (Exception e) {
			}
			return b;
		}

		/**
		 * 文本组件中是否具备复制的条件
		 * 
		 * @return true为具备
		 */
		public boolean isCanCopy() {
			boolean b = false;
			int start = this.getSelectionStart();
			int end = this.getSelectionEnd();
			if (start != end)
				b = true;
			return b;
		}

		public void mouseClicked(MouseEvent e) {
		}

		public void mouseEntered(MouseEvent e) {
		}

		public void mouseExited(MouseEvent e) {
		}

		public void mousePressed(MouseEvent e) {
			if (e.getButton() == MouseEvent.BUTTON3) {
				copy.setEnabled(isCanCopy());
				paste.setEnabled(isClipboardString());
				cut.setEnabled(isCanCopy());
				pop.show(this, e.getX(), e.getY());
			}
		}

		public void mouseReleased(MouseEvent e) {
		}

	}
	
	
}
