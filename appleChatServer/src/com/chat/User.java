package com.chat;

public class User{
    private String name;//用户名
    private String ip;//用户IP
    private int state;//1已连接 0离线 2在线
    private String serverPort;//文件服务端口,不需要
    
    public User(String name,String ip) {
        this.name = name;
        this.ip=ip;
        state = 0;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

	public String getServerPort() {
		return serverPort;
	}

	public void setServerPort(String serverPort) {
		this.serverPort = serverPort;
	}
    
}
