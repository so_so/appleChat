package com.chat;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;



public class ServerChatFrame extends JFrame {
	
	    
	    private TextAreaMenu contentArea;
	    
	    private TextAreaMenu msg_Area;
	    private JTextField txt_max;
	    private JTextField txt_port;
	    private JButton btn_start;
	    private JButton btn_stop;
	    private JButton btn_send;
	   // private JButton btn_send_file;
	    private JPanel northPanel;
	    private JPanel southPanel;
	    private JPanel sendPanel;
	    private JScrollPane rightPanel;
	    private JScrollPane leftPanel;
	   
	    private JSplitPane centerSplit;
	 
	    private JList userList;
	   
	    private static DefaultListModel all_listModel;
	    
	    private int sendfor_who = 0;//监听左边jlist，保存给哪个用户发消息

	public ServerChatFrame() {
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		SelecTry selectIndex = new SelecTry();
     
        contentArea = new TextAreaMenu();
        contentArea.setEditable(false);
        contentArea.setForeground(Color.blue);
      
        msg_Area = new TextAreaMenu();
        txt_max = new JTextField("10");
        txt_port = new JTextField("6666");
        btn_start = new JButton("启动");
        btn_stop = new JButton("停止");
        btn_send = new JButton("群发");
       // btn_send_file = new JButton("文件");
        btn_stop.setEnabled(false);
     
        all_listModel = new DefaultListModel();
        
        userList = new JList(all_listModel);
        userList.addListSelectionListener(selectIndex);

     
        southPanel = new JPanel(new BorderLayout());
        southPanel.setPreferredSize(new Dimension(this.WIDTH, 60));
        sendPanel = new JPanel(new BorderLayout());
        southPanel.setBorder(new TitledBorder("写通知"));
        
        southPanel.add(msg_Area, "Center");
        sendPanel.add(btn_send, BorderLayout.NORTH);
        //sendPanel.add(btn_send_file, BorderLayout.SOUTH);

        southPanel.add(sendPanel, "East");

        leftPanel = new JScrollPane(userList);
        leftPanel.setBorder(new TitledBorder("用户列表"));



        rightPanel = new JScrollPane(contentArea);
        rightPanel.setBorder(new TitledBorder("消息通知"));

        centerSplit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, leftPanel,
                rightPanel);
        centerSplit.setDividerLocation(150);


        northPanel = new JPanel();
        northPanel.setLayout(new GridLayout(1, 6));
        northPanel.add(new JLabel("       人数上限"));
        northPanel.add(txt_max);
        northPanel.add(new JLabel("         端口"));
        northPanel.add(txt_port);
        northPanel.add(btn_start);
        northPanel.add(btn_stop);
        northPanel.setBorder(new TitledBorder("配置信息"));
        
        this.setTitle("服务器");
      
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(
				this.getClass().getResource("/resource/2.png")));
        this.setLayout(new BorderLayout());
        this.add(northPanel, "North");
        this.add(centerSplit, "Center");
        
        this.add(southPanel, "South");
        this.setSize(600, 400);
        this.setResizable(false);
       
        int screen_width = Toolkit.getDefaultToolkit().getScreenSize().width;
        int screen_height = Toolkit.getDefaultToolkit().getScreenSize().height;
        this.setLocation((screen_width - this.getWidth()) / 2,
                (screen_height - this.getHeight()) / 2);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);

	}

	

	public TextAreaMenu getContentArea() {
		return contentArea;
	}

	public void setContentArea(TextAreaMenu contentArea) {
		this.contentArea = contentArea;
	}

	public TextAreaMenu getMsg_Area() {
		return msg_Area;
	}

	public void setMsg_Area(TextAreaMenu msg_Area) {
		this.msg_Area = msg_Area;
	}

	public JTextField getTxt_max() {
		return txt_max;
	}

	public void setTxt_max(JTextField txt_max) {
		this.txt_max = txt_max;
	}

	public JTextField getTxt_port() {
		return txt_port;
	}

	public void setTxt_port(JTextField txt_port) {
		this.txt_port = txt_port;
	}

	public JButton getBtn_start() {
		return btn_start;
	}

	public void setBtn_start(JButton btn_start) {
		this.btn_start = btn_start;
	}

	public JButton getBtn_stop() {
		return btn_stop;
	}

	public void setBtn_stop(JButton btn_stop) {
		this.btn_stop = btn_stop;
	}

	

	public JPanel getNorthPanel() {
		return northPanel;
	}

	public void setNorthPanel(JPanel northPanel) {
		this.northPanel = northPanel;
	}

	
	public JScrollPane getRightPanel() {
		return rightPanel;
	}

	public void setRightPanel(JScrollPane rightPanel) {
		this.rightPanel = rightPanel;
	}

	public JScrollPane getLeftPanel() {
		return leftPanel;
	}

	public void setLeftPanel(JScrollPane leftPanel) {
		this.leftPanel = leftPanel;
	}

	public JSplitPane getCenterSplit() {
		return centerSplit;
	}

	public void setCenterSplit(JSplitPane centerSplit) {
		this.centerSplit = centerSplit;
	}

	public JList getUserList() {
		return userList;
	}

	public void setUserList(JList userList) {
		this.userList = userList;
	}

	
	public static DefaultListModel getAll_listModel() {
		return all_listModel;
	}

	public static void setAll_listModel(DefaultListModel all_listModel) {
		ServerChatFrame.all_listModel = all_listModel;
	}

	public int getSendfor_who() {
		return sendfor_who;
	}

	public void setSendfor_who(int sendfor_who) {
		this.sendfor_who = sendfor_who;
	}

	

	public JButton getBtn_send() {
		return btn_send;
	}



	public void setBtn_send(JButton btn_send) {
		this.btn_send = btn_send;
	}



	public JPanel getSouthPanel() {
		return southPanel;
	}



	public void setSouthPanel(JPanel southPanel) {
		this.southPanel = southPanel;
	}



	public JPanel getSendPanel() {
		return sendPanel;
	}



	public void setSendPanel(JPanel sendPanel) {
		this.sendPanel = sendPanel;
	}












	/**
	 * 左边用户列表
	 * @author Administrator
	 *
	 */
	class SelecTry implements ListSelectionListener {
		int change = 0, who;
        @Override
        public void valueChanged(ListSelectionEvent e) {
           
            sendfor_who = userList.getSelectedIndex();
        }

	}

	/**
	 * 带有功能菜单的JTextArea
	 * 
	 */
	class TextAreaMenu extends JTextArea implements MouseListener {

		private static final long serialVersionUID = -2308615404205560110L;

		private JPopupMenu pop = null; // 弹出菜单

		private JMenuItem copy = null, paste = null, cut = null; // 三个功能菜单

		public TextAreaMenu() {
			super();
			init();
		}

		private void init() {
			this.addMouseListener(this);
			pop = new JPopupMenu();
			pop.add(copy = new JMenuItem("复制"));
			pop.add(paste = new JMenuItem("粘贴"));
			pop.add(cut = new JMenuItem("剪切"));
			copy.setAccelerator(KeyStroke.getKeyStroke('C', InputEvent.CTRL_MASK));
			paste.setAccelerator(KeyStroke.getKeyStroke('V', InputEvent.CTRL_MASK));
			cut.setAccelerator(KeyStroke.getKeyStroke('X', InputEvent.CTRL_MASK));
			copy.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					action(e);
				}
			});
			paste.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					action(e);
				}
			});
			cut.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					action(e);
				}
			});
			this.add(pop);
		}

		/**
		 * 菜单动作
		 * 
		 * @param e
		 */
		public void action(ActionEvent e) {
			String str = e.getActionCommand();
			if (str.equals(copy.getText())) { // 复制
				this.copy();
			} else if (str.equals(paste.getText())) { // 粘贴
				this.paste();
			} else if (str.equals(cut.getText())) { // 剪切
				this.cut();
			}
		}

		public JPopupMenu getPop() {
			return pop;
		}

		public void setPop(JPopupMenu pop) {
			this.pop = pop;
		}

		/**
		 * 剪切板中是否有文本数据可供粘贴
		 * 
		 * @return true为有文本数据
		 */
		public boolean isClipboardString() {
			boolean b = false;
			Clipboard clipboard = this.getToolkit().getSystemClipboard();
			Transferable content = clipboard.getContents(this);
			try {
				if (content.getTransferData(DataFlavor.stringFlavor) instanceof String) {
					b = true;
				}
			} catch (Exception e) {
			}
			return b;
		}

		/**
		 * 文本组件中是否具备复制的条件
		 * 
		 * @return true为具备
		 */
		public boolean isCanCopy() {
			boolean b = false;
			int start = this.getSelectionStart();
			int end = this.getSelectionEnd();
			if (start != end)
				b = true;
			return b;
		}

		@Override
		public void mouseClicked(MouseEvent e) {
		}

		@Override
		public void mouseEntered(MouseEvent e) {
		}

		@Override
		public void mouseExited(MouseEvent e) {
		}

		@Override
		public void mousePressed(MouseEvent e) {
			if (e.getButton() == MouseEvent.BUTTON3) {
				copy.setEnabled(isCanCopy());
				paste.setEnabled(isClipboardString());
				cut.setEnabled(isCanCopy());
				pop.show(this, e.getX(), e.getY());
			}
		}

		@Override
		public void mouseReleased(MouseEvent e) {
		}

	}

}
